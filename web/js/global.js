$(function () {
    $('select').selectize({
        dropdownParent: 'body'
    });
    $('input[type="radio"]').change(function () {
        $('.contact_form-type--empty').css('display', 'inline-block');
        if ($(this).is(':checked')) {
            $('.contact_form-type--empty:has( + input[type="radio"]:checked )').css('display', 'none');
        }
    });

    $('.slideshow').flexslider({
        pauseOnHover: false
    });
    $('.snippet_slideshow').flexslider({
        animation: "slide",
        itemWidth: 520,
        controlNav: false
    });
});