# Installation
Invoke `lando start` on root project.

# Utils 
## Deprecated
Command to watch SASS files:
`lando bundler exec sass -r sass-globbing --watch scss:css`

## Compass
Invoke compass throought bundle command:
`lando bundle exec compass watch`
